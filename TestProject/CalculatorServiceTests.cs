using SampleApplication;
using System;
using Xunit;

namespace TestProject
{
    public class CalculatorServiceTests
    {
        [Fact]
        public void AddTest()
        {
            var calServ = new CalculatorService();

            var i = 10;
            var j = 10;
            var response = calServ.Add(i, j);
            Assert.Equal(20, response);

        }
    }
}
